package com.sdacademy.sbproject.controller;

import com.mashape.unirest.http.exceptions.UnirestException;
import com.sdacademy.sbproject.service.WeatherDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class WeatherDataController {

    @Autowired
    WeatherDataService weatherDataService;

    @GetMapping("/available/dailyForecastsUrl/all")
    public String getAllAvailableDailyForecasts() {
        return weatherDataService.getAllAvailableDailyForecastUrl();
    }

    @GetMapping("/available/dailyForecastsUrl/oneDay")
    public String getOneDayAvailableDailyForecast() throws UnirestException {
        return weatherDataService.getOneDayWeatherForecast();
    }

    @GetMapping("/available/dailyForecastsUrl/fiveDays")
    public String getFiveDayAvailableDailyForecast() throws UnirestException {
        return weatherDataService.getFiveDayWeatherForecast();
    }

    @GetMapping("/available/dailyForecastsUrl/tenDays")
    public String getTenDayAvailableDailyForecast() throws UnirestException {
        return weatherDataService.getTenDayWeatherForecast();
    }

    @GetMapping("/available/dailyForecastsUrl/fifteenDays")
    public String getFifteenDayAvailableForecast() throws UnirestException {
        return weatherDataService.getFifteenDayWeatherForecast();
    }
}
