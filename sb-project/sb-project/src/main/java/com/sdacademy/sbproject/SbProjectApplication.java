package com.sdacademy.sbproject;

import org.openqa.selenium.chrome.ChromeDriver;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;

@SpringBootApplication
public class SbProjectApplication {

	public static void main(String[] args) {
		SpringApplication.run(SbProjectApplication.class, args);
	}

	@EventListener({ApplicationReadyEvent.class})
	public void applicationReadyEvent() {
		final String localHost = "http://localhost:8080/available/dailyForecastsUrl/all";
		final String driverPath = "C:\\Users\\iesus\\IdeaProjects\\sb-project\\chromedriver_win32 (1)\\chromedriver1.exe";
		System.setProperty("webdriver.chrome.driver", driverPath);
		ChromeDriver chromeDriver = new ChromeDriver();
		chromeDriver.get(localHost);
	}
}
