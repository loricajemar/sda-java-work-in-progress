package com.sdacademy.sbproject.service;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import com.sdacademy.sbproject.model.AccuweatherWeatherDataEndpoint;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class WeatherDataService {

    @Autowired
    AccuweatherWeatherDataEndpoint accuweatherWeatherDataEndpoint;

    private final String apiKey = "3HC3cADQtFktrDlrwKjeQspHgaQGTr2F";
    private final String locationKey = "263780";    //San Pablo City, Laguna
    private final boolean metric = true;

    public String getAllAvailableDailyForecastUrl() {
        return accuweatherWeatherDataEndpoint.toString();
    }

    public String getOneDayWeatherForecast() throws UnirestException {
        final String oneDayWeatherForecastUrl =
                "http://dataservice.accuweather.com/forecasts/v1/daily/1day/{locationKey}";

        HttpResponse<JsonNode> responseOneDay = Unirest.get(oneDayWeatherForecastUrl)
                .header("accept", "application/json")
                .routeParam("locationKey", locationKey)
                .queryString("apikey", apiKey)
                .queryString("metric", metric)
                .asJson();
        return String.valueOf(responseOneDay.getBody());
    }

    public String getFiveDayWeatherForecast() throws UnirestException {
        final String fiveDayWeatherForecastUrl =
                "http://dataservice.accuweather.com/forecasts/v1/daily/5day/{locationKey}";

        HttpResponse<JsonNode> responseFiveDays = Unirest.get(fiveDayWeatherForecastUrl)
                .header("accept", "application/json")
                .routeParam("locationKey", locationKey)
                .queryString("apikey", apiKey)
                .queryString("metric", metric)
                .asJson();
        return String.valueOf(responseFiveDays.getBody());
    }

    public String getTenDayWeatherForecast() throws UnirestException {
        String tenDayWeatherForecastUrl =
                accuweatherWeatherDataEndpoint.getTenDays(); // wanted to access the value in the application.properties
                                                            //receiving "Code: Unauthorized" with Status: OK 200

        HttpResponse<JsonNode> responseTenDays = Unirest.get(tenDayWeatherForecastUrl)
                .header("accept", "application/json")
                .routeParam("locationKey", locationKey)
                .queryString("apikey", apiKey)
                .queryString("metric", metric)
                .asJson();
        return String.valueOf(responseTenDays.getBody());
    }

    public String getFifteenDayWeatherForecast() throws UnirestException {
        final String tenDayWeatherForecastUrl =
                "http://dataservice.accuweather.com/forecasts/v1/daily/15day/{locationKey}";

        HttpResponse<JsonNode> responseFifteenDays = Unirest.get(tenDayWeatherForecastUrl)
                .header("accept", "application/json")
                .routeParam("locationKey", locationKey)
                .queryString("apikey", apiKey)
                .queryString("metric", metric)
                .asJson();
        return String.valueOf(responseFifteenDays.getBody());
    }
}
