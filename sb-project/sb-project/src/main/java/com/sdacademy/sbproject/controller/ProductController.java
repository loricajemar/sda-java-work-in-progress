package com.sdacademy.sbproject.controller;

import com.sdacademy.sbproject.model.Product;
import com.sdacademy.sbproject.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class ProductController {

    @Autowired
    ProductService productService;

    //get all products
    @GetMapping("/products/all")
    public List<Product> getAllProducts() {
        return productService.getAllProducts();
    }

    //get product by Id
    @GetMapping("/products/{id}")
    public Product getProductById(@PathVariable String id) {
        return productService.getProductById(id);
    }

    //update a product by Id
    @PutMapping("/products/{id}")
    public Product updateProductPrice(@PathVariable String id, @RequestBody Product updatedProduct) {
        return productService.updateProductPrice(id, updatedProduct.getPrice());
    }

    //add a new product
    @PostMapping("/products")
    public Product addNewProduct(@RequestBody Product product) {
        return productService.addNewProduct(product);
    }

    //remove a product by Id
    @DeleteMapping("/products/{id}")
    public ResponseEntity<Product> deleteProductById(@PathVariable String id) {
        if (productService.getProductById(id) == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        else {
            productService.deleteProductById(id);
        }
        return new ResponseEntity<>(HttpStatus.OK);
    }

}
