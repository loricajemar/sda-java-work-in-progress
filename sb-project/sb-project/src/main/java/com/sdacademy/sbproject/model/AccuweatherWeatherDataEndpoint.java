package com.sdacademy.sbproject.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties(prefix = "accuweather.daily-forecast")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class AccuweatherWeatherDataEndpoint {
    private String oneDay;
    private String fiveDays;
    private String tenDays;
    private String fifteenDays;

    public String getOneDay() {
        return oneDay;
    }

    public String getFiveDays() {
        return fiveDays;
    }

    public String getTenDays() {
        return tenDays;
    }

    public String getFifteenDays() {
        return fifteenDays;
    }

    @Override
    public String toString() {
        return "AccuweatherWeatherDataEndpoint{" +
                "\n\nGET - " + getOneDay() + '\'' +
                ", \n\nGET - " + getFiveDays() + '\'' +
                ", \n\nGET - " + getTenDays() + '\'' +
                ", \n\nGET - " + getFifteenDays() + '\'' +
                '}';
    }
}


