package com.sdacademy.sbproject.service;

import com.sdacademy.sbproject.model.Product;
import org.springframework.stereotype.Service;

import java.security.SecureRandom;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class ProductService {

    private static Map<String, Product> productMap = new HashMap<>();

    static {
        Product phone = new Product();
        phone.setId("1");
        phone.setModelName("vivo Y30");
        phone.setPrice(10_000);
        phone.setManufacturer("vivo");
        productMap.put(phone.getId(), phone);

        Product laptop2 = new Product();
        laptop2.setId("2");
        laptop2.setModelName("tuf gaming");
        laptop2.setPrice(46_000);
        laptop2.setManufacturer("asus");
        productMap.put(laptop2.getId(), laptop2);

        Product laptop = new Product();
        laptop.setId("3");
        laptop.setModelName("aspire 5");
        laptop.setPrice(34_000);
        laptop.setManufacturer("acer");
        productMap.put(laptop.getId(), laptop);

        Product tablet = new Product();
        tablet.setId("4");
        tablet.setModelName("iPad");
        tablet.setPrice(78_000);
        tablet.setManufacturer("Apple");
        productMap.put(tablet.getId(), tablet);
    }

    //get all products
    public List<Product> getAllProducts() {
        return productMap.values().stream().collect(Collectors.toList());
    }

    //get product by Id
    public Product getProductById(String id) {
        return productMap.get(id);
    }

    private SecureRandom secureRandom = new SecureRandom();

    //add new product
    public Product addNewProduct(Product newProduct) {
        Product product = new Product();
        product.setId(String.valueOf(Math.abs(secureRandom.nextInt())));
        product.setModelName(newProduct.getModelName());
        product.setPrice(newProduct.getPrice());
        product.setManufacturer(newProduct.getManufacturer());
        productMap.put(product.getId(), product);
        return product;
    }

    //update product by Id
    public Product updateProductPrice(String id, double newPrice) {
        Product product = null;
        if (productMap.containsKey(id)) {
            product = productMap.get(id);
            product.setPrice(newPrice);
        }
        return product;
    }

    //delete product by Id
    public void deleteProductById(String id) {
        if (productMap.containsKey(id)) {
            productMap.remove(id);
        }
    }
}
