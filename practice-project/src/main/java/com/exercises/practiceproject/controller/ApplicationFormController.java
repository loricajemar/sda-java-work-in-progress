package com.exercises.practiceproject.controller;

import com.exercises.practiceproject.model.ApplicationForm;
import com.exercises.practiceproject.service.ApplicationFormService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import javax.validation.Valid;

@RestController
@Validated
public class ApplicationFormController {

    @Autowired
    ApplicationFormService applicationFormService;

    @PostMapping("/applicants/form")
    public ApplicationForm newApplicant(@RequestBody @Valid ApplicationForm applicant) {
        return applicationFormService.createApplication(applicant);
    }
}
