package com.exercises.practiceproject.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Past;
import java.time.LocalDate;
import java.util.List;

public class ApplicationForm {

    private String id;

    @NotEmpty(message = "Field required.")
    private String firstName;

    @NotEmpty(message = "Field required.")
    private String lastName;

    @NotEmpty(message = "Field required.")
    private String gender;

    @Past
    @JsonFormat(pattern = "yyyy-MM-dd")
    private LocalDate birthday;

    @NotEmpty(message = "Field required.")
    private String phoneNumber;

    @NotEmpty(message = "Field required.")
    private String address;

    @NotEmpty(message = "Field required.")
    private String lastSchoolAttended;

    @Min(value = 1L)
    @Max(value = 3L)
    private long gpa;

    @NotEmpty(message = "Field is required")
    private String courseMajor;

    @Past
    @JsonFormat(pattern = "yyyy-MM-dd")
    private LocalDate dateOfGraduation;

    private List<String> awards;

    @NotEmpty(message = "Field required.")
    private String courseIntendedToEnroll;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public LocalDate getBirthday() {
        return birthday;
    }

    public void setBirthday(LocalDate birthday) {
        this.birthday = birthday;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getLastSchoolAttended() {
        return lastSchoolAttended;
    }

    public void setLastSchoolAttended(String lastSchoolAttended) {
        this.lastSchoolAttended = lastSchoolAttended;
    }

    public long getGpa() {
        return gpa;
    }

    public void setGpa(long gpa) {
        this.gpa = gpa;
    }

    public String getCourseMajor() {
        return courseMajor;
    }

    public void setCourseMajor(String courseMajor) {
        this.courseMajor = courseMajor;
    }

    public LocalDate getDateOfGraduation() {
        return dateOfGraduation;
    }

    public void setDateOfGraduation(LocalDate dateOfGraduation) {
        this.dateOfGraduation = dateOfGraduation;
    }

    public List<String> getAwards() {
        return awards;
    }

    public void setAwards(List<String> awards) {
        this.awards = awards;
    }

    public String getCourseIntendedToEnroll() {
        return courseIntendedToEnroll;
    }

    public void setCourseIntendedToEnroll(String courseIntendedToEnroll) {
        this.courseIntendedToEnroll = courseIntendedToEnroll;
    }
}
