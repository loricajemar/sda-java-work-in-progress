package com.exercises.practiceproject.service;

import com.exercises.practiceproject.model.ApplicationForm;
import org.springframework.stereotype.Service;

import java.security.SecureRandom;
import java.util.HashMap;
import java.util.Map;

@Service
public class ApplicationFormService {

    private Map<String, ApplicationForm> applicationFormMap = new HashMap<>();
    {

    }

    public ApplicationForm getApplicantById(String id) {
        return applicationFormMap.get(id);
    }

    private SecureRandom secureRandom = new SecureRandom();

    public ApplicationForm createApplication(ApplicationForm newApplicant) {
        ApplicationForm applicant = new ApplicationForm();
        applicant.setId(String.valueOf(Math.abs(secureRandom.nextInt())));
        applicant.setFirstName(newApplicant.getFirstName());
        applicant.setLastName(newApplicant.getLastName());
        applicant.setGender(newApplicant.getGender());
        applicant.setBirthday(newApplicant.getBirthday());
        applicant.setPhoneNumber(newApplicant.getPhoneNumber());
        applicant.setAddress(newApplicant.getAddress());
        applicant.setLastSchoolAttended(newApplicant.getLastSchoolAttended());
        applicant.setGpa(newApplicant.getGpa());
        applicant.setCourseMajor(newApplicant.getCourseMajor());
        applicant.setDateOfGraduation(newApplicant.getDateOfGraduation());
        applicant.setAwards(newApplicant.getAwards());
        applicant.setCourseIntendedToEnroll(newApplicant.getCourseIntendedToEnroll());
        applicationFormMap.put(applicant.getId(), applicant);
        return newApplicant;
    }
}
