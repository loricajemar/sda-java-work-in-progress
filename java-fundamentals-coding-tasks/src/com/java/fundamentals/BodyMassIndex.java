package com.java.fundamentals;

import java.text.DecimalFormat;
import java.util.Scanner;

public class BodyMassIndex {
    static float weight;
    static int height;
    static float bmi;

    public static void main(String[] args) {
        System.out.println("To begin calculating your BMI,\nplease type your weight in kg:");
        Scanner scanner = new Scanner(System.in);
        DecimalFormat decimalFormat = new DecimalFormat("#.##");
        weight = scanner.nextFloat();
        System.out.println("Type your height in cm:");
        height = scanner.nextInt();
        float sqHeightInMeter = (float) Math.pow(height/100.0, 2);
        bmi = weight / sqHeightInMeter;
        System.out.println("Your BMI is: " + decimalFormat.format(bmi));

        if (bmi >= 18.5 && bmi <= 24.9) {
            System.out.println("\nRemarks: BMI optimal");
        }
        else {
            System.out.println("\nRemarks: BMI not optimal");
        }
    }
}
