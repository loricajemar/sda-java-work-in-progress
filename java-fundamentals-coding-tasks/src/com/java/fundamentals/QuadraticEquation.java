package com.java.fundamentals;

import java.util.Scanner;

public class QuadraticEquation {
    static int a;
    static int b;
    static int c;
    static double delta;

    public static void main(String[] args) {
        System.out.println("Please input the values for a, b, and c.");
        Scanner scanner = new Scanner(System.in);
        a = scanner.nextInt();
        System.out.println("You've entered " + a + " for the value of a.");
        System.out.println("\nEnter the value for b: ");
        b = scanner.nextInt();
        System.out.println("You've entered " + b + " for the value of b.");
        System.out.println("\nEnter the value for c: ");
        c = scanner.nextInt();
        System.out.println("You've entered " + c + " for the value of c.");

        computeDeltaValue();
        computeQuadraticRoot();

        if(delta < 0) {
            System.out.println("\nYou obtained a DELTA NEGATIVE");
        }
    }

    private static void computeQuadraticRoot() {
        double x1;
        double x2;
        x1 = ((-(b) - Math.sqrt(delta))) / ((2) * (a));
        x2 = ((-(b) + Math.sqrt(delta))) / ((2) * (a));
    }

    private static void computeDeltaValue() {
        delta = Math.pow(b, 2) - 4 * (a) * (c);
        System.out.println("Delta value: " + delta);
    }
}
