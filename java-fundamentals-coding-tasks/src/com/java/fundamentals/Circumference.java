package com.java.fundamentals;

import java.text.DecimalFormat;
import java.util.Scanner;

public class Circumference {

    public static void main(String[] args) {
        getCircumference();
    }

    //formula: C = 2πr
    private static void getCircumference() {
        final float pi = 3.14F;
        float diameter;

        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter diameter of the circle to calculate circumference: ");
        diameter = scanner.nextFloat();
        DecimalFormat decimalFormat = new DecimalFormat("#.##");
        System.out.println("Circumference is: " + decimalFormat.format(2 * pi * (diameter/2)));
    }
}
